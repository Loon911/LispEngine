function tokenize(chars) {
	return chars.replace(/\(/g, ' ( ').replace(/\)/g, ' ) ').trim().match(/(?:[^\s"]+|"[^"]*")+/g)
	//return chars.replace(/\(/g, ' ( ').replace(/\)/g, ' ) ').trim().split(/\s+/)
}


function atom(token) {
	if (!isNaN(parseFloat(token))) {
		return parseFloat(token)
	}
	else {
		if (lisp_core[token] == true) {
			return token
		}
		else if(env[token] != undefined) {
			return token
		}
		else if(token[0] == '"') {
			return token
		}
		else {
			//new_lisp_variable[token] = false
			return token
		}
	}
}

function read_from_tokens(tokens) {
	if (tokens.length == 0) {
		return "Unexpected EOF while reading"
	}
	var token = tokens.shift()
	if ('(' == token) {
		var L = []
		while (tokens[0] != ")") {
			L.push(read_from_tokens(tokens))
		}
		tokens.shift()
		return L
	}
	else if (')' == token) {
		return "Unexpected"
	}
	else {
		return token
	}
}

console.log(read_from_tokens(['(','1','2','(','3','4','(','5',')',')',')']))
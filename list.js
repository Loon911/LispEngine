var run = function(program) {
	var res = parse(program)
	if (res != false)
		return eval_lisp(res,'global_env')
	else
		console.log("Parameters don't match")
} 

function parse(program) {
	var res = tokenize(program)
	if (res != false)
		return read_from_tokens(res)
	else
		return false
}

function tokenize(chars) {

	if (chars.match(/\(/g) == null || chars.match(/\(/g).length == chars.match(/\)/g).length)
		return chars.replace(/\(/g, ' ( ').replace(/`\s\(/g, ' `( ').replace(/\)/g, ' ) ').trim().match(/(?:[^\s"']+|"[^"]*"|'[^']*')+/g)
	else
		return false
	//return chars.replace(/\(/g, ' ( ').replace(/\)/g, ' ) ').trim().split(/\s+/)
}

function read_from_tokens(tokens, quote = false) {
	if (tokens.length == 0) {
		return "Unexpected EOF while reading"
	}
	var token = tokens.shift()
	if ('(' == token) {
		var L = []
		while (tokens[0] != ")") {
			L.push(read_from_tokens(tokens,quote))
		}
		tokens.shift()
		return L
	}
	else if ('`(' == token && quote == false) {
		var L = [{value: "`", type: "backquote"}]
		while (tokens[0] != ")") {
			L.push(read_from_tokens(tokens,true))
		}
		tokens.shift()
		return L
	}
	else if ('`(' == token && quote == true) {
		var L = [{value: "list", type: "symbol"}]
		while (tokens[0] != ")") {
			L.push(read_from_tokens(tokens,true))
		}
		tokens.shift()
		return L
	}
	else if (')' == token) {
		return "Unexpected"
	}
	else {
		return atom(token)
	}
}

function atom(token) {
	if (!isNaN(parseFloat(token))) {
		return {value: parseFloat(token), type: "number"}
	}
	else if(token[0] == '"' || token[0] == "'") {
		token = token.slice(1, token.length - 1)
		return {value: token, type: "string"}
	}
	else {
		return {value: token, type: "symbol"}	
	}
}

// Make ID:
function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

var env = {
	'global_env' : {
		'id' : "global_env",
		'+' : function(x) {
			var sum = x[0]
			for (var i = 1; i < x.length; i++) {
				sum += x[i]
			}
			return sum
		},
		'-' : function(x) {
			var sum = x[0]
			for (var i = 1; i < x.length; i++) {
				sum = sum - x[i]
			}
			return sum
		},
		'*' : function(x) {
			var sum = x[0]
			for (var i = 1; i < x.length; i++) {
				sum = sum * x[i]
			}
			return sum
		},
		'/' : function(x) {
			var sum = x[0]
			for (var i = 1; i < x.length; i++) {
				sum = sum / x[i]
			}
			return sum
		},
		'=' : function(x) {
			return x[0] == x[1]
		},
		'<' : function(x) {
			return x[0] < x[1]
		},
		'<=' : function(x) {
			return x[0] <= x[1]
		},
		'>' : function(x) {
			return x[0] > x[1]
		},
		'>=' : function(x) {
			return x[0] >= x[1]
		},
		'print' : function(x) {
			return x
		},
		'pi' : Math.PI,
		'first' : function(x) {
			return x[0]
		},
		'numberp' : function(x) {
			if (!isNaN(x[0]))
				return true
			else
				return false
		},
		'symbolp' : function(x) {
			if (typeof x[0] === "string")
				return true
			else
				return false
		},
		// html getters
		'get_id' : function(x) {
			return document.getElementById(x[0])
		},
		'set_id' : function (x) {
			x[0][x[1]] = x[2]
		}
	}
}

// run("(defparameter hello (get_id 'meow'))")
// run("(set_id hello 'innerText' 'bla')")


function find_outer_env(x, env_id) {
	var ladder_up = env_id
	var current_env = env[ladder_up]
	while (ladder_up != "global_env") {
		if (current_env[x] != undefined) {
			return ladder_up
		}
		else {
			ladder_up = current_env.parent
			current_env = env[ladder_up]
		}
	}
	if (ladder_up == "global_env" && env["global_env"][x] != undefined) {
		return ladder_up
	}
	else {
		return undefined
	}
}

function eval_lisp(x,env_id) {
	//console.log(x)
	if (!(x instanceof Array)) {
		// do something with the value
		if (x.type == "symbol") {
			var search_outer = find_outer_env(x.value,env_id)
			var query = env[search_outer]
			if (query != undefined) {
				return query[x.value]
			}
			else {
				return x.value
			}
		}
		else if (x.type == "number") {
			return x.value
		}
		else if (x.type == "string") {
			return x.value
		}
		else { // could be garbage returns whole {}
			console.log("Returning object... for some reason")
			return x
		}
	}
	else if (x[0].value == "print_env") {
		for (var env_ids in env) {
			if (env_ids == "global_env") {
				for (var global_env_ids in env[env_ids]) {
					console.log(`global_env.${global_env_ids} = ${env[env_ids][global_env_ids]}`);
				}
			}
			console.log(`id.${env_ids} = ${env[env_ids]}`);
		}
	}
	else if (x[0].value == "if") {
		if (eval_lisp(x[1], env_id)) {
			return eval_lisp(x[2], env_id)
		}
		else {
			return eval_lisp(x[3], env_id)
		}
	}
	else if (x[0].value == "quote") {
		return x[1]
	}
	else if (x[0].value == "eval") {
		var query = eval_lisp(x[1],env_id)
		return eval_lisp(query,env_id)
	}
	else if (x[0].value == "defparameter") {
		env["global_env"][x[1].value] = eval_lisp(x[2], env_id)
	}
	else if (x[0].value == "setf") {
		var current = find_outer_env(x[1].value,env_id)
		if (current == undefined) {
			current = "global_env"
		}
		env[current][x[1].value] = eval_lisp(x[2], env_id)
		return eval_lisp(x[2], env_id)
	}
	else if (x[0].value == "var") {
		var var_list = x[1]
		for (var i = 0; i < var_list.length; i++) {
			var current = find_outer_env(var_list[i][0].value, env_id)
			if (current == undefined) {
				current = "global_env"
			}
			env[current][var_list[i][0].value] = eval_lisp(var_list[i][1].value, env_id)
		}
		return eval_lisp(x[2], env_id)
	}
	else if (x[0].value == "progn") {
		var count = 0
		for (var i = 1; i < x.length; i++) {
			eval_lisp(x[i],env_id)
		}
		return eval_lisp(x[x.length - 1], env_id)
	}
	else if (x[0].value == "`") { // only 1 nested
		function nest_list(pos, new_list) { // pos to avoid the `
			var list = new Array()
			for (var i = pos; i < new_list.length; i++) {
				if (!(new_list[i] instanceof Array)) {
					if (new_list[i]['value'][0] == ",") {
						var variable = new_list[i]['value'].slice(1)
						var current = find_outer_env(variable,env_id)
						if (current == undefined) {
							console.error("There is an undefined variable: " + variable)
							return undefined
						}
						var value = env[current][variable]
						list.push(value)
					}
					else {
						list.push(new_list[i])
					}
				}
				else {
					list.push(nest_list(0, new_list[i]))
				}
			}
			return list
		}
		return nest_list(1,x)
	}
	else if (x[0].value == "list") {
		var list = new Array()
		for (var i = 1; i < x.length; i++) {
			var res = eval_lisp(x[i], env_id)
			if (!(res instanceof Array)) {
				if (res.type) {
					list.push(res)
				}
				else {
					list.push(atom(res))
				}
			}
			else {
			 	list.push(res)
			}
			//list.push(atom(res))
		}
		return list
	}
	else if (x[0].value == "defmacro") {
		/*
		need to add this:
			Object.defineProperty(obj, 'key', {
			  enumerable: false,
			  configurable: false,
			  writable: false,
			  value: 'static'
			});
		*/
		var defmacro_env = {}
		defmacro_env.type = "defmacro"
		defmacro_env.id = uuidv4()
		defmacro_env.parent = env_id

		// Check if its proper
		if (x[1] instanceof Array ) {
			console.error("ERROR: You forgot to give a name for your defmacro.")
			return undefined
		}

		var parameter_list = []
		for (var i = 0; i < x[2].length; i++) {
			var value_eval = x[2][i].value
			parameter_list.push(value_eval)
			defmacro_env[value_eval] = null
		}
		defmacro_env.parameter_list = parameter_list

		defmacro_env.expression = x[3]

		env["global_env"][x[1].value] = defmacro_env
		env[defmacro_env.id] = defmacro_env

		return defmacro_env
	}
	else if (x[0].value == "lambda") {
		// init
		var lambda_env = {}
		lambda_env.type = "lambda"
		lambda_env.id = uuidv4()
		lambda_env.parent = env_id

		// Check if its proper
		if (!(x[1] instanceof Array )) {
			console.error("ERROR: You cannot give a name for lambda.")
			return undefined
		}

		// get parameters
		var parameter_list = []
		for (var i = 0; i < x[1].length; i++) {
			var value_eval = x[1][i].value
			parameter_list.push(value_eval)
			lambda_env[value_eval] = null
		}
		lambda_env.parameter_list = parameter_list

		// assignemt express
		lambda_env.expression = x[2]

		// Added it to env
		env[lambda_env.id] = lambda_env
		return lambda_env
	}
	else {
		// Not core now try evaling first part of list
		var proc = eval_lisp(x[0], env_id)
		console.log(proc)

		if (proc.type == "lambda") {
			if (proc.parameter_list.length != x.length - 1) {
				if (proc.parameter_list.length < x.length)
					return "Too many arguments in call to " + proc.id
				else
					return "Too few arguments in call to " + proc.id
			}

			for (var i = 1; i < x.length; i++) {
				var current_parameter = eval_lisp(x[i], env_id) // Lambda eval prameters
				var look_up = proc.parameter_list[i - 1]
				proc[look_up] = current_parameter
			}

			return eval_lisp(proc.expression, proc.id)
		}
		else if (proc.type == "defmacro") {
			if (proc.parameter_list.length != x.length - 1) {
				if (proc.parameter_list.length < x.length)
					return "Too many arguments in call to " + proc.id
				else
					return "Too few arguments in call to " + proc.id
			}

			for (var i = 1; i < x.length; i++) {
				var current_parameter = x[i] // Defmacro don't eval prameters
				var look_up = proc.parameter_list[i - 1]
				proc[look_up] = current_parameter
			}

			//console.log(eval_lisp(proc.expression, proc.id))
			return eval_lisp(eval_lisp(proc.expression, proc.id), proc.id)
			//return eval_lisp(proc.expression, proc.id) //debugging
		}
		else {
			var args = new Array()
			for (var i = 1; i < x.length; i++) {
				args.push(eval_lisp(x[i],env_id))
			}
			var result = null
			try {
				result = args.length != 0 ? proc(args) : proc
			}
			catch(error) {
				result = "EXECUTE ERROR: " + error + "\n" + proc
			}
			return result
		}
	}
}

run("(defmacro defun (name args body) `(defparameter ,name (lambda ,args ,body)))") // Run once
//run("(defmacro doc_id (tag_name type set) `(set_id (get_id ,tag_name) ,type ,set))") // broken
run("(defmacro doc_id (tag_name type1 set) (list set_id (list get_id tag_name) type1 set))")
//run("(defmacro doc_id (tag_name type set) (list + tag_name type set))")

// var readline = require('readline');
// var rl = readline.createInterface({

//   input: process.stdin,
//   output: process.stdout,
//   terminal: false,
//   prompt: "? "
// });

// rl.prompt()

// rl.on('line', function(line){
//     console.log(run(line.toString()));
//     rl.prompt()
// })


//console.log(parse("('hello there')"))
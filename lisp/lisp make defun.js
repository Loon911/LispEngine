var run = function(program) {
	return eval_lisp(parse(program))
} 

function parse(program) {
	return read_from_tokens(tokenize(program))
}

function tokenize(chars) {
	return chars.replace(/\(/g, ' ( ').replace(/\)/g, ' ) ').trim().match(/(?:[^\s"]+|"[^"]*")+/g)
	//return chars.replace(/\(/g, ' ( ').replace(/\)/g, ' ) ').trim().split(/\s+/)
}

function read_from_tokens(tokens) {
	if (tokens.length == 0) {
		return "Unexpected EOF while reading"
	}
	var token = tokens.shift()
	if ('(' == token) {
		var L = []
		while (tokens[0] != ")") {
			L.push(read_from_tokens(tokens))
		}
		tokens.shift()
		return L
	}
	else if (')' == token) {
		return "Unexpected"
	}
	else {
		return atom(token)
	}
}

function atom(token) {
	if (!isNaN(parseFloat(token))) {
		return {value: parseFloat(token), type: "number"}
	}
	else {
		if (lisp_core[token] == true) {
			return {value: token, type: 'core'}
		}
		else if(env[token] != undefined) {
			return {value: token, type: "function"}
		}
		else if(token[0] == '"') {
			return {value: token, type: "string"}
		}
		else {
			//new_lisp_variable[token] = false
			return {value: token, type: "variable"}
		}
	}
}

var lisp_core = {
	"if" : true,
	"defparameter" : true,
	"lambda" : true
}

var new_lisp_variable = {}

var env = {
	'+' : function(x) {
		var sum = x[0]
		for (var i = 1; i < x.length; i++) {
			sum += x[i]
		}
		return sum
	},
	'-' : function(x) {
		var sum = x[0]
		for (var i = 1; i < x.length; i++) {
			sum = sum - x[i]
		}
		return sum
	},
	'*' : function(x) {
		var sum = x[0]
		for (var i = 1; i < x.length; i++) {
			sum = sum * x[i]
		}
		return sum
	},
	'/' : function(x) {
		var sum = x[0]
		for (var i = 1; i < x.length; i++) {
			sum = sum / x[i]
		}
		return sum
	},
	'<' : function(x) {
		return x[0] < x[1]
	},
	'<=' : function(x) {
		return x[0] <= x[1]
	},
	'>' : function(x) {
		return x[0] > x[1]
	},
	'>=' : function(x) {
		return x[0] >= x[1]
	},
	'print' : function(x) {
		return x
	},
	'pi' : Math.PI,
	'first' : function(x) {
		return x[0][0]
	},
	'numberp' : function(x) {
		if (!isNaN(x[0]))
			return true
		else
			return false
	},
	'symbolp' : function(x) {
		if (typeof x[0] === "string")
			return true
		else
			return false
	}
}

function eval_lisp(x) {
	//console.log(x)
	if (!(x instanceof Array)) {
		if (x.type == "variable") {
			if (new_lisp_variable[x.value] == true) {
				return env[x.value]
			}
			else {
				return "Unbound variable: " + x.value
			}
		}
		else if (x.type == "number") {
			return x.value
		}
		else if (x.type == "function") {
			return env[x.value]
		}
		else if (x.type == "Anonymous-Function") {
			return x
		}
		else { // no type
			return x;
		}
	}
	else if (x[0].type == "core") {
		if (x[0].value == "if") {
			if (eval_lisp(x[1])) {
				return eval_lisp(x[2])
			}
			else {
				return eval_lisp(x[3])
			}
		}
		else if (x[0].value == "defparameter") {
			if (x[1].type == "variable") {
				new_lisp_variable[x[1].value] = true
				env[eval_lisp(x[1].value)] = eval_lisp(x[2])
				return x[1].value
			}
			else {
				console.log(x[1].value + " is not an expected type SYMBOL. " + x[1].value + " is a type " + x[1].type)
			}
		}
		else if (x[0].value == "lambda") {
			// expression
			var parameter_list = []
			for (var i = 0; i < x[1].length; i++) {
				var parameter_name = x[1][i].value + ""
				parameter_list.push(parameter_name)
			}
			var anonymous_function = {expression: x[2], type: "Anonymous-Function"}
			anonymous_function.parameter = parameter_list
			return anonymous_function
		}
	}
	else {// changed this to read anoymous function
		var proc = eval_lisp(x[0])
		// Execute javascript function

			// Here you check the local file
		if (proc.type == "Anonymous-Function") {
			console.log(x.length)
			console.log(proc.parameter.length)

			if (x.length - 1  != proc.parameter.length) {
				return "Too few arguments in call to: " + JSON.stringify(proc);
			}

			for (var i = 1; i < x.length; i++) {
				new_lisp_variable[proc.parameter[i-1]] = true
				var var_name = proc.parameter[i-1]
				env[var_name] = x[i].value
			}
			
			return eval_lisp(proc.expression)
		}
		else {
			var args = new Array()
			for (var i = 1; i < x.length; i++) {
				args.push(eval_lisp(x[i]))
			}
			var result = null
			try {
				result = args.length != 0 ? proc(args) : proc
			}
			catch(error) {
				result = "EXECUTE ERROR: " + error + "\n" + proc
			}
			return result
		}

	}
}


/* REPL */
// var readline = require('readline');
// var rl = readline.createInterface({

//   input: process.stdin,
//   output: process.stdout,
//   terminal: false,
//   prompt: "? "
// });

// rl.prompt()

// rl.on('line', function(line){
//     console.log(run(line.toString()));
//     rl.prompt()
// })


// console.log(eval_lisp(parse("(lambda (x) (lambda (y) (+ x y)))")))
//console.log(eval_lisp(parse("((lambda (x y) (+ x y)) 1 2)")))
console.log(eval_lisp(parse("(defparameter add2 (lambda (x y) (+ x y)))")))
console.log(eval_lisp(parse("(add2 2 3)")))
// console.log(env)
// console.log(parse("(first (20 3 4 1 1 1 1 1 10))"))
// console.log(parse("(+ (+ 3 4 1 1 1 1 1 10) 1 2)"))
// console.log()
// console.log(parse("(defun (x y) (+ x y))"))
// console.log()
// console.log(parse("(+ 2 3)"))
// console.log()
// console.log(eval_lisp(parse("(+ 2 (+ 3 2))")))
// console.log()
// console.log(eval_lisp(parse("(+ 2 3 2 2 1)")))
//console.log(eval_lisp(parse("(first (20 3 4 1 1 1 1 1 10))")))
//console.log(eval_lisp(parse("(* pi (* 2 2))")))

//eval_lisp(parse("(defparameter r 10)"))
//console.log(eval_lisp(parse("(* pi (* r r))")))

//console.log(eval_lisp(parse("(last (3 2 (first ((add (1 2 3)) 2 3))))")))
//console.log(eval_lisp(parse("(/ 100 50 2 2)")))
//console.log(eval_lisp(parse("(numberp (/ 100 50 2 2))")))
//console.log(eval_lisp(parse("(pi)")))







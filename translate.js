class Lisp {
	constructor() {
		this.environment = {
			'+' : function(array) {
				if (array.length >= 2) {
					let js_string = "("
					for (let i = 0; i < array.length; i++) {
						if ((i + 1) == array.length) {
							js_string += array[i].value + ')'
							return {type: 'number', value: js_string}
						}
						js_string += array[i].value + ' + '
					}
					return {type: 'number', value: js_string}
				}
				else {
					console.error("Error: Must have at least two arguments for the function +")
					return null
				}
			}
		}
	}

	run(string) {
		let result
		console.log("--------[ Input ]--------")
		result = this.tokenize(string)
		console.log(result)
		console.log("--------[ Array ]--------")
		result = this.make_array(result)
		console.log(result)
		console.log("--------[ Output ]--------")
		result = this.lisp_eval(result)
		console.log(result)
		console.log("--------[ Evaled ]--------")
		result = eval(result.value)
		console.log(result)
		return result
	}

	is_punctuation(char) {
		let punc_list = ["(",")"]
		for (let i = 0; i < punc_list.length; i++) {
			if (punc_list[i] == char) {
				return true
			}
		}

		return false
	}

	atom(char) {
		if (!isNaN(parseFloat(char))) {
			let parsed = parseFloat(char)
			return {type: "number", value: parsed}
		}
		// else if (char[0] == "'" && char[char.length - 1] == "'") {
		// 	return {type : "string", value : char}
		// }
		else if (char == "TRUE") {
			return {type: "bool", value: char}
		}
		else if (char == "FALSE") {
			return {type: "bool", value: char}
		}
		else {
			return {type : "symbol", value : char}
		}
		console.alert("Something passed through the atom")
	}

	ignore_string(array, string) {
		if (string != "") {
			string = string.toUpperCase()
			string = this.atom(string)
			array.push(string)
		}
	}

	tokenize(string) {
		let token_array = new Array()
		let current_word = ""
		let isString = false
		for (let i = 0; i < string.length; i++) {

			if (!isString) {
				if (string[i] == " ") {
					this.ignore_string(token_array, current_word)
					current_word = ""
				}
				else if (string[i] == "(") {
					this.ignore_string(token_array, current_word)
					current_word = ""
					token_array.push({type : "punc", value : "("})
				}
				else if (string[i] == ")") {
					this.ignore_string(token_array, current_word)
					current_word = ""
					token_array.push({type : "punc", value : ")"})
				}
				else if(string[i] == "\t") {
					// do nothing
				}
				else if (string[i] == "\n") {
					// do nothing
				}
				else if ((i + 1) == string.length && string[i] != ")") {
					current_word += string[i]
					this.ignore_string(token_array, current_word)
				}
				else {
					current_word += string[i]
				}
			}
			else {
				current_word += string[i]
			}

			if (string[i] == '"') {
				if (isString) {
					isString = false
					token_array.push({type : "string", value: current_word.slice(0, current_word.length - 1)})
					current_word = ""
				}
				else {
					isString = true
					current_word = "" 
				}
			}
		}
		return token_array
	}

	make_array(array) {
		let string = ""
		for (let i = 0; i < array.length; i++) {
			let current = array[i]
			let next = array[i + 1]
			if (array[i] == undefined) {
				return string
			}
			if (array.length == 1) {
				return array[0]
			}
			else if (current.type != "punc" && next != undefined && next.type != "punc") {
				string += JSON.stringify(current) + ","
			}
			else if (current.type != "punc" && next != undefined && next.value == "(") {
				string += JSON.stringify(current) + ","
			}
			else if (current.value == ")" && next != undefined && next.type != "punc") {
				string += "],"
			}
			else if (current.value == ")" && next != undefined && next.value == "(") {
				string += "],"
			}
			else if (current.value == "(") {
				string += "["
			}
			else if (current.value == ")") {
				string += "]"
			}
			else {
				string += JSON.stringify(current)
			}
		}
		try {
			return eval(string)
		}
		catch(error) {
			console.error(`Error Parenthese Not Matching: ${error}`)
			return [null]
		}
	}

	lisp_eval(expression) {
		if (!(expression instanceof Array)) {
			if (expression.type == "symbol" && this.environment[expression.value]) {
				return this.environment[expression.value]
			}
			return expression
		}
		else if (expression[0].type == "symbol" && expression[0].value == "DEFPARAMETER") {
			if (expression[1].type == "symbol") {
				return {type: expression, value: `this.environment['${expression[1].value}'] = ${JSON.stringify(this.lisp_eval(expression[2]))}`}
				/* examples */
				// console.log("EMWOD:?")
				// console.log(this.environment["HELLO"])
				// console.log("LAM")
				// console.log((() => {
				// 	return this.environment["HELLO"]
				// })())
			}
			else {
				console.error("Error: In ${expression[0].value} ${expression[1].value} must be a symbol")
			}
		}
		else if (expression[0].type == "symbol" && expression[0].value == "QUOTE") {
			let quote = new Array()
			for (let i = 0; i < expression[1].length; i++) {
				quote.push(expression[1][i])
			}
			console.log("QUOTE:")
			console.log(quote)
			return quote
		}
		else if (expression[0].type == "symbol" && expression[0].value == "EVAL") {
			console.log(expression[1])
			let evaling = this.lisp_eval(expression[1])
			evaling = this.lisp_eval(evaling) //hack?
			console.log("Evaling:")
			console.log(evaling)
			return evaling
		}
		else if (expression[0].type == "symbol" && expression[0].value == "LIST") {
			let list = new Array()
			for (let i = 1; i < expression.length; i++) {
				list.push(this.lisp_eval(expression[i]))
			}
			console.log("LIST:")
			console.log(list)
			return list
		}
		else if (expression[0]) {
			let args = new Array()
			for (let i = 1; i < expression.length; i++) {
				args.push(this.lisp_eval(expression[i]))
			}
			console.log("Args:")
			console.log(args)

			let result
			if (args.length == 0)
				result = this.environment[expression[0].value]
			else
				result = this.environment[expression[0].value](args)


			console.log("Result:")
			console.log(result)

			return result
		}
	}
}
	

let lisp = new Lisp()
// lisp.run("(+ 1 2 3 4 (+ 10 10))")
// lisp.run("(defparameter hello (+ 1 2 3 4 (+ 10 10)))")
// lisp.run("(defparameter hello (list 2 10))")
// lisp.run("(list hello hello 2)")
// lisp.run("(quote (+ 3 3))")
// lisp.run("(defparameter hello (quote (+ 2 2)))")
lisp.run(`(defparameter hello (+ 10 2))`)
// lisp.run("(list hello)")
lisp.run("(+ hello 2)")
// lisp.run("(list 1 23)")
// lisp.run("(+ (+ 2 2) 2)")
 // lisp.run('(+ (eval hello) 3)')
 // lisp.run('(+ (eval (quote (+ 3 3 ))) 3)')




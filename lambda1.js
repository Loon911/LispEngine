var run = function(program) {
	return eval_lisp(parse(program),'global_env')
} 

function parse(program) {
	return read_from_tokens(tokenize(program))
}

function tokenize(chars) {
	return chars.replace(/\(/g, ' ( ').replace(/\)/g, ' ) ').trim().match(/(?:[^\s"']+|"[^"]*"|'[^']*')+/g)
	//return chars.replace(/\(/g, ' ( ').replace(/\)/g, ' ) ').trim().split(/\s+/)
}

function read_from_tokens(tokens) {
	if (tokens.length == 0) {
		return "Unexpected EOF while reading"
	}
	var token = tokens.shift()
	if ('(' == token) {
		var L = []
		while (tokens[0] != ")") {
			L.push(read_from_tokens(tokens))
		}
		tokens.shift()
		return L
	}
	else if (')' == token) {
		return "Unexpected"
	}
	else {
		return atom(token)
	}
}

function atom(token) {
	if (!isNaN(parseFloat(token))) {
		return {value: parseFloat(token), type: "number"}
	}
	else if(token[0] == '"' || token[0] == "'") {
		return {value: token, type: "string"}
	}
	else {
		return {value: token, type: "symbol"}	
	}
}

// Make ID:
function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

var env = {
	'global_env' : {
		'id' : "global_env",
		'+' : function(x) {
			var sum = x[0]
			for (var i = 1; i < x.length; i++) {
				sum += x[i]
			}
			return sum
		},
		'-' : function(x) {
			var sum = x[0]
			for (var i = 1; i < x.length; i++) {
				sum = sum - x[i]
			}
			return sum
		},
		'*' : function(x) {
			var sum = x[0]
			for (var i = 1; i < x.length; i++) {
				sum = sum * x[i]
			}
			return sum
		},
		'/' : function(x) {
			var sum = x[0]
			for (var i = 1; i < x.length; i++) {
				sum = sum / x[i]
			}
			return sum
		},
		'<' : function(x) {
			return x[0] < x[1]
		},
		'<=' : function(x) {
			return x[0] <= x[1]
		},
		'>' : function(x) {
			return x[0] > x[1]
		},
		'>=' : function(x) {
			return x[0] >= x[1]
		},
		'print' : function(x) {
			return x
		},
		'pi' : Math.PI,
		'first' : function(x) {
			return x[0]
		},
		'numberp' : function(x) {
			if (!isNaN(x[0]))
				return true
			else
				return false
		},
		'symbolp' : function(x) {
			if (typeof x[0] === "string")
				return true
			else
				return false
		}
	}
}

function find_outer_env(x, env_id) {
	var ladder_up = env_id
	var current_env = env[ladder_up]
	while (ladder_up != "global_env") {
		if (current_env[x] != undefined) {
			return ladder_up
		}
		else {
			ladder_up = current_env.parent
			current_env = env[ladder_up]
		}
	}
	if (ladder_up == "global_env" && env["global_env"][x] != undefined) {
		return ladder_up
	}
	else {
		return undefined
	}
}

function eval_lisp(x,env_id) {
	//console.log(x)
	if (!(x instanceof Array)) {
		// do something with the value
		if (x.type == "symbol") {
			// console.log(x)
			// console.log("type: " + x.value + " env_id: " + env_id + " Parent: " + parent)
			var search_outer = find_outer_env(x.value,env_id)
			var query = env[search_outer]
			if (query != undefined) {
				return query[x.value]
			}
			else {
				return x.value
			}
		}
		else if (x.type == "number") {
			return x.value
		}
		else if (x.type == "string") {
			return x.value
		}
		else { // could be garbage returns whole {}
			return x
		}
	}
	else if (x[0].value == "print_env") {
		for (var env_ids in env) {
			if (env_ids == "global_env") {
				for (var global_env_ids in env[env_ids]) {
					console.log(`global_env.${global_env_ids} = ${env[env_ids][global_env_ids]}`);
				}
			}
			console.log(`id.${env_ids} = ${env[env_ids]}`);
		}
	}
	else if (x[0].value == "if") {
		if (eval_lisp(x[1]), env_id) {
			return eval_lisp(x[2], env_id)
		}
		else {
			return eval_lisp(x[3], env_id)
		}
	}
	else if (x[0].value == "quote") {
		return x[1]
	}
	else if (x[0].value == "eval") {
		var query = eval_lisp(x[1],env_id)
		return eval_lisp(query,env_id)
	}
	else if (x[0].value == "defparameter") {
		env["global_env"][x[1].value] = eval_lisp(x[2], env_id)
	}
	else if (x[0].value == "setf") {
		//console.log("env_id: " + env_id)
		env[env_id][x[1].value] = eval_lisp(x[2], env_id)
		return eval_lisp(x[2], env_id)
	}
	else if (x[0].value == "progn") {
		var count = 0
		for (var i = 1; i < x.length; i++) {
			// if (i == x.length - 1)   // broken (progn (setf x 5) (setf x (+ x x)) (setf x (* x x)))
			// 	return eval_lisp(x[i],env_id)
			// else
				eval_lisp(x[i],env_id)
		}
		//return eval_lisp(x[x.length - 1], env_id)
	}
	else if (x[0].value == "lambda") {
		// init
		
		var lambda_env = {}
		lambda_env.type = "lambda"
		lambda_env.id = uuidv4()
		lambda_env.parent = env_id
		// get parameters

		var parameter_list = []
		for (var i = 0; i < x[1].length; i++) {
			var value_eval = x[1][i].value
			parameter_list.push(value_eval)
			lambda_env[value_eval] = null
		}
		lambda_env.parameter_list = parameter_list
		// assignemt express

		lambda_env.expression = x[2]
		// Added it to env

		env[lambda_env.id] = lambda_env
		return lambda_env
	}
	else {
		// Not core now try evaling first part of list
		var proc = eval_lisp(x[0], env_id)

		if (proc.type == "lambda") {
			if (proc.parameter_list.length != x.length - 1) {
				if (proc.parameter_list.length < x.length)
					return "Too many arguments in call to " + proc.id
				else
					return "Too few arguments in call to " + proc.id
			}

			for (var i = 1; i < x.length; i++) {
				var current_parameter = eval_lisp(x[i], env_id)
				var look_up = proc.parameter_list[i - 1]
				proc[look_up] = current_parameter
			}

			return eval_lisp(proc.expression, proc.id)
		}
		else {
			var args = new Array()
			for (var i = 1; i < x.length; i++) {
				args.push(eval_lisp(x[i],env_id))
			}
			var result = null
			try {
				result = args.length != 0 ? proc(args) : proc
			}
			catch(error) {
				result = "EXECUTE ERROR: " + error + "\n" + proc
			}
			return result
		}
	}
}


var readline = require('readline');
var rl = readline.createInterface({

  input: process.stdin,
  output: process.stdout,
  terminal: false,
  prompt: "? "
});

rl.prompt()

rl.on('line', function(line){
    console.log(run(line.toString()));
    rl.prompt()
})


//console.log(parse("('hello there')"))
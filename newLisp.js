function run(string) {
	let result
	console.log("--------[ Input ]--------")
	result = tokenize(string)
	console.log(result)
	console.log("--------[ Array ]--------")
	result = make_array(result)
	console.log(result)
	result = lisp_eval(result)
	console.log("--------[ Output ]--------")
	console.log(JSON.stringify(result))
	// console.log("--------[ Evaled ]--------")
	// if (result) {
	// 	result = eval(result.value)
	// 	console.log(result)
	// 	return result
	// }
	// console.log(result)
	return result
}

function is_punctuation(char) {
	let punc_list = ["(",")"]
	for (let i = 0; i < punc_list.length; i++) {
		if (punc_list[i] == char) {
			return true
		}
	}

	return false
}

function atom(char) {
	if (!isNaN(parseFloat(char))) {
		return {type: "number", value: parseFloat(char)}
	}
	// else if (char[0] == "'" && char[char.length - 1] == "'") {
	// 	return {type : "string", value : char}
	// }
	else if (char == "TRUE") {
		return {type: "bool", value: char}
	}
	else if (char == "FALSE") {
		return {type: "bool", value: char}
	}
	else {
		return {type : "symbol", value : char}
	}
	console.alert("Something passed through the atom")
}

function ignore_string(array, string) {
	if (string != "") {
		string = string.toUpperCase()
		string = atom(string)
		array.push(string)
	}
}

function tokenize(string) {
	let token_array = new Array()
	let current_word = ""
	let isString = false
	for (let i = 0; i < string.length; i++) {

		if (!isString) {
			if (string[i] == " ") {
				ignore_string(token_array, current_word)
				current_word = ""
			}
			else if (string[i] == "(") {
				ignore_string(token_array, current_word)
				current_word = ""
				token_array.push({type : "punc", value : "("})
			}
			else if (string[i] == ")") {
				ignore_string(token_array, current_word)
				current_word = ""
				token_array.push({type : "punc", value : ")"})
			}
			else if(string[i] == "\t") {
				// do nothing
			}
			else if (string[i] == "\n") {
				// do nothing
			}
			else if ((i + 1) == string.length && string[i] != ")") {
				current_word += string[i]
				ignore_string(token_array, current_word)
			}
			else {
				current_word += string[i]
			}
		}
		else {
			current_word += string[i]
		}

		if (string[i] == '"') {
			if (isString) {
				isString = false
				token_array.push({type : "string", value: current_word.slice(0, current_word.length - 1)})
				current_word = ""
			}
			else {
				isString = true
				current_word = "" 
			}
		}
	}
	return token_array
}

function make_array(array) {
	let string = ""
	for (let i = 0; i < array.length; i++) {
		let current = array[i]
		let next = array[i + 1]
		if (array[i] == undefined) {
			return string
		}
		if (array.length == 1) {
			return array[0]
		}
		else if (current.type != "punc" && next != undefined && next.type != "punc") {
			string += JSON.stringify(current) + ","
		}
		else if (current.type != "punc" && next != undefined && next.value == "(") {
			string += JSON.stringify(current) + ","
		}
		else if (current.value == ")" && next != undefined && next.type != "punc") {
			string += "],"
		}
		else if (current.value == ")" && next != undefined && next.value == "(") {
			string += "],"
		}
		else if (current.value == "(") {
			string += "["
		}
		else if (current.value == ")") {
			string += "]"
		}
		else {
			string += JSON.stringify(current)
		}
	}
	try {
		return eval(string)
	}
	catch(error) {
		console.error(`Error Parenthese Not Matching: ${error}`)
		return [null]
	}
}

let lisp_env = {
	'PARENT' : null,
	'+' : function(array) {
		if (array.length >= 2) {
			let js_string = "("
			for (let i = 0; i < array.length; i++) {
				if ((i + 1) == array.length) {
					js_string += array[i].value + ')'
					return {type: 'expression', value: js_string}
				}
				js_string += array[i].value + ' + '
			}
			return {type: 'expression', value: js_string}
		}
		else {
			console.error("Error: Must have at least two arguments for the function +")
			return null
		}
	},
	'-' : function(array) {
		if (array.length >= 2) {
			let js_string = "("
			for (let i = 0; i < array.length; i++) {
				if ((i + 1) == array.length) {
					js_string += array[i].value + ')'
					return {type: 'expression', value: js_string}
				}
				js_string += array[i].value + ' - '
			}
			return {type: 'expression', value: js_string}
		}
		else {
			console.error("Error: Must have at least two arguments for the function -")
			return {type: 'symbol' , value: null}
		}
	},
	'*' : function(array) {
		if (array.length >= 2) {
			let js_string = "("
			for (let i = 0; i < array.length; i++) {
				if ((i + 1) == array.length) {
					js_string += array[i].value + ')'
					return {type: 'expression', value: js_string}
				}
				js_string += array[i].value + ' * '
			}
			return {type: 'expression', value: js_string}
		}
		else {
			console.error("Error: Must have at least two arguments for the function *")
			return {type: 'symbol' , value: null}
		}
	},
	'/' : function(array) {
		if (array.length >= 2) {
			let js_string = "("
			for (let i = 0; i < array.length; i++) {
				if ((i + 1) == array.length) {
					js_string += array[i].value + ')'
					return {type: 'expression', value: js_string}
				}
				js_string += array[i].value + ' / '
			}
			return {type: 'expression', value: js_string}
		}
		else {
			console.error("Error: Must have at least two arguments for the function /")
			return {type: 'symbol' , value: null}
		}
	}
}

function lisp_eval(expression) {
	// console.log(expression)
	if (!(expression instanceof Array)) {
		if (expression.type == 'symbol') {
			let result = lisp_env[expression.value]
			if (result != undefined) {
				return {type: "expression", value: result}
			}

			return {type: "expression", value: expression.value}
		}
		else if (expression.type == 'string') {
			return {type: "string", value: `'${expression.value}'`}
		}
		else if (expression.type == 'lambda') {
			console.log("--------[ Return lambda ]--------")
			console.log(expression)
			return expression
		}
		else {
			return expression
		}
	}

	if (expression[0].type == "symbol" && expression[0].value == "DEFPARAMETER") {
		if (expression[1].type == "symbol") {
			let result = lisp_eval(expression[2])
			let js_string = `${expression[1].value} = ${result.value};`
			console.log(js_string)
			return {type: 'expression', value: js_string}
		}
		else {
			console.error("Error: In ${expression[0].value} ${expression[1].value} must be a symbol")
		}
	}
	else if (expression[0].type == "symbol" && expression[0].value == "LIST") {
		let list = new Array()
		for (let i = 1; i < expression.length; i++) {
			list.push(expression[i])
		}
		console.log("LIST:")
		console.log(list)
		return lisp_eval(list)
	}
	else if (expression[0].type == "symbol" && expression[0].value == "DEFMACRO") {
		if (expression[1] instanceof Array) {
			let parameters = new Array()
			console.log("PARAMS:")
			for (let i = 0; i < expression[1].length; i++) {
				console.log(expression[1][i])
				parameters.push(expression[1][i].value)
			}
			let result = {
				type : "function",
				parameters : parameters,
				value : `(function() {
					return ${JSON.stringify(lisp_eval(expression[2]).value)};
				})();`
			}
			return lisp_eval(result)
		}
		else {
			console.error("Error: In ${expression[0].value} ${expression[1]} must be a array")
		}
	}
	else if (expression[0].type == "symbol" && expression[0].value == "LAMBDA") {
		if (expression[1] instanceof Array) {
			let parameters = new Array()
			let string_parameters = ""
			console.log("PARAMS:")
			for (let i = 0; i < expression[1].length; i++) {
				console.log(expression[1][i])
				parameters.push(expression[1][i].value)
				if (i == expression[1].length - 1) {
					string_parameters += expression[1][i].value
				}
				else {
					string_parameters += expression[1][i].value + ","
				}
			}
			console.log(string_parameters)
			let result = {
				type : "lambda",
				parameters : parameters,
				value : `(function(${string_parameters}) {
					return ${lisp_eval(expression[2]).value};
				})`
			}
			return lisp_eval(result)
		}
		else {
			console.error("Error: In ${expression[0].value} ${expression[1]} must be a array")
		}
	}
	else if (expression[0]) {
		console.log("--------[ Evaling... ]--------")
		let func = lisp_eval(expression[0])
		console.log("FUNCTION:")
		console.log(func)
		let args = new Array()
		for (let i = 1; i < expression.length; i++) {
			args.push(lisp_eval(expression[i]))
		}

		if (func.type == "expression"){
			let result
			if (args.length == 0) {
				result = lisp_eval(lisp_env[expression[0].value]())
			}
			else {
				result = lisp_eval(lisp_env[expression[0].value](args))
			}

			console.log("--------[ Test ]--------")
			console.log("Result: " + JSON.stringify(result))

			let nonEvaled = result.value
			//let evaled = eval(result.value)

			console.log(JSON.stringify(atom(nonEvaled)))
			console.log("Eval: " + JSON.stringify({type: atom(nonEvaled).type, value: nonEvaled}))

			return {type: atom(nonEvaled).type, value: nonEvaled}
		}
		else if (func.type == "defmacro") {
			console.log(`"--------[ Function Eval]--------"`)
			let args = new Array()
			for (let i = 1; i < expression.length; i++) {
				args.push(lisp_eval(expression[i]))
			}
			console.log("ARGS:")
			console.log(args)
			if (args.length != func.parameters.length) {
				console.error(`Error: parameters do not match arguments in ${JSON.stringify(func)}`)
				return
			}
			for (let i = 0; i < func.parameters.length; i++) {
				let current_parameter = func.parameters[i]
				let reg_right = new RegExp(`${current_parameter} `, 'g')
				let reg_mid = new RegExp(` ${current_parameter} `, 'g')
				let reg_left = new RegExp(` ${ current_parameter}`, 'g')
				func.value = func.value.replace(reg_right, `${args[i].value} `)
				func.value = func.value.replace(reg_mid, ` ${args[i].value} `)
				func.value = func.value.replace(reg_left, ` ${args[i].value}`)
			}
			console.log("RESULT:")
			console.log(JSON.stringify(func))
			return {type: `expression`, value: eval(func.value)}
		}
		else if (func.type == "lambda") {
				console.log(`"--------[ Function Eval]--------"`)
				let args = new Array()
				for (let i = 1; i < expression.length; i++) {
					args.push(lisp_eval(expression[i]))
				}
				console.log("ARGS:")
				console.log(args)
				if (args.length != func.parameters.length) {
					console.error(`Error: parameters do not match arguments in ${JSON.stringify(func)}`)
					return
				}
				let string_args = "("
				for (let i = 0; i < args.length; i++) {
					if (i == args.length - 1) {
						string_args += args[i].value
					}
					else {
						string_args += args[i].value + ","
					}
				}
				string_args += ")"
				console.log(string_args)
				func.value += string_args
				console.log("RESULT:")
				console.log(JSON.stringify(func))
				return {type: `expression`, value: func.value}
		}
		else {
			console.error(`Error: Cannot compile function: ${func}`)
		}
	}
	else {
		console.error(`Error: ${expression[0].value} is not a symbol. It is a ${expression[0].type}.`)
		return null
	}
}

//run('      \ttrue    (+ (  "cunt fag"  "n g"     - 10  hey  5 ) 3      \n          	)')
//run('( 1 2 3 ( 4 5 ( 7)) 6 )')
//run('(+ 1 ((20 ( 30))))')
//run("(average (+ 3 4) (- 3 4) 5)")
run(`(- 3 3 (- 3 3))`)
//run("(/ (* (- (+ 1 2 (+ 3 4)) 8) 10) 40)")
//run("(defparameter hello 20)")
// run("(defparameter hello 12)")
// console.log(eval(run(`(defparameter hello "hello there")`).value))
// //console.log("MATH: " + (run("(+ hello hello hello)") + 100))
// run("(defparameter time (+ 3 (* 6 6 ( / 3 3))))") 
// eval(run("(defparameter hello 'meow')").value)
// run(`(+ 1 'hello')`)
// eval(run("(defparameter time hello)").value)
// eval(run("(defparameter time (+ 2 hello))").value)
// run("time")
// run("((lambda (time) (+ time time)) 10)") // wrong!!!!!!!!!!!!!!!!!!!!!!!
// run("time")
// run("(+ time hello)")
// console.log(eval(run("((lambda (x y) (+ x y 3)) 10 20)").value))
// run("((lambda (x y) (+ x y 3)) 10 20)")
// run("(lambda (x y) (lambda (z) (+ x y z)))")
// console.log(eval(run("((lambda (x y) ((lambda (z) (+ x y z)) 5)) 10 20)").value))
//run("(hello)")
//console.log("MEOW: " + lisp_env['HELLO'])
// eval(run(`(defparameter hello "fuck thou")`).value)
// console.log(eval(run(`((lambda (x y) (+ x y 3 hello)) 10 20)`).value))
//run("(+ hello 3)")
// run("(+ time hello)")
// console.log(lisp_env.test())
// console.log(eval("var hello = 10"))
//run("hello")
//run("hello")
//run(`bla`)
//run(`("hello")`)
//run(`(1 hello true false)`)
/*
run(`(defparameter defun 
		(lambda (name args body)
			(list defparameter ,name (list lambda ,args ,body))))`)
*/

// run("(defparameter hello (list + 3 10))")
// run("(lambda() (defparameter meow 10))")


my favorite language is Lisp, so I wanted to make a Lisp in Javascript. When creating this project I’v had partial success. I created the most basic Lisp functions but I have failed to fully implement defmacro. Also this language is not optimized very well like having the inclusion of a while loop for recession as Javascript has a stack limit. 

Each file has a different version with different types of tests. I started with simple defun for Lisp all the way to making defmacros. The latest file is scope.js . In this file I was trying to work out how to make a better scope for Lamdas functions.

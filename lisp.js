var run = function(program) {
	return eval(parse(program))
} 

function parse(program) {
	return read_from_tokens(tokenize(program))
}

function tokenize(chars) {
	return chars.replace(/\(/g, ' ( ').replace(/\)/g, ' ) ').trim().split(/\s+/)
}

function read_from_tokens(tokens) {
	if (tokens.length == 0) {
		return "Unexpected EOF while reading"
	}
	var token = tokens.shift()
	if ('(' == token) {
		var L = []
		while (tokens[0] != ")") {
			L.push(read_from_tokens(tokens))
		}
		tokens.shift()
		return L
	}
	else if (')' == token) {
		return "Unexpected"
	}
	else {
		return atom(token)
	}
}

function atom(token) {
	if (!isNaN(parseFloat(token))) {
		return parseFloat(token)
	}
	else {
		return token
	}
}

var env = {
	'+' : function(x) {
		var sum = x[0]
		for (var i = 1; i < x.length; i++) {
			sum += x[i]
		}
		return sum
	},
	'-' : function(x) {
		var sum = x[0]
		for (var i = 1; i < x.length; i++) {
			sum = sum - x[i]
		}
		return sum
	},
	'*' : function(x) {
		var sum = x[0]
		for (var i = 1; i < x.length; i++) {
			sum = sum * x[i]
		}
		return sum
	},
	'/' : function(x) {
		var sum = x[0]
		for (var i = 1; i < x.length; i++) {
			sum = sum / x[i]
		}
		return sum
	},
	'<' : function(x) {
		return x[0] < x[1]
	},
	'<=' : function(x) {
		return x[0] <= x[1]
	},
	'>' : function(x) {
		return x[0] > x[1]
	},
	'>=' : function(x) {
		return x[0] >= x[1]
	},
	'print' : function(x) {
		return x
	},
	'pi' : Math.PI,
	'first' : function(x) {
		return x[0][0]
	},
	'numberp' : function(x) {
		if (!isNaN(x[0]))
			return true
		else
			return false
	},
	'symbolp' : function(x) {
		if (typeof x[0] === "string")
			return true
		else
			return false
	}
}

function eval(x) {
	if ((typeof x) === "string") {
		if (env[x])
			return env[x]
		else
			console.error("Not a function: " + x)
	}
	else if (!(x instanceof Array)) {
		return x
	}
	else if (x[0] == 'defparameter') {
		env[x[1]] = eval(x[2])
	}
	else if (x[0] == "if") {
		if (eval(x[1])) {
			return eval(x[2])
		}
		else {
			return eval(x[3])
		}
	}
	else if ((typeof x[0]) == "number") { // list
		return x
	}
	else {
		var proc = eval(x[0])
		var args = new Array()
		for (var i = 1; i < x.length; i++) {
			args.push(eval(x[i]))
		}
		return args.length != 0 ? proc(args) : proc
	}
}


/* REPL */
var readline = require('readline');
var rl = readline.createInterface({

  input: process.stdin,
  output: process.stdout,
  terminal: false,
  prompt: "? "
});

rl.prompt()

rl.on('line', function(line){
    console.log(run(line.toString()));
    rl.prompt()
})


//console.log(eval(parse("(first (20 3 4 1 1 1 1 1 10))")))
//console.log(eval(parse("(* pi (* 2 2))")))

//eval(parse("(defparameter r 10)"))
//console.log(eval(parse("(* pi (* r r))")))

//console.log(eval(parse("(last (3 2 (first ((add (1 2 3)) 2 3))))")))
//console.log(eval(parse("(/ 100 50 2 2)")))
//console.log(eval(parse("(numberp (/ 100 50 2 2))")))
//console.log(eval(parse("(pi)")))







function run(string) {
	let result
	console.log("--------[ Input ]--------")
	result = tokenize(string)
	console.log(result)
	console.log("--------[ Array ]--------")
	result = make_array(result)
	console.log(result)
	console.log("--------[ Output ]--------")
	result = lisp_eval(result)
	console.log(result)
	return result
}

function is_punctuation(char) {
	let punc_list = ["(",")"]
	for (let i = 0; i < punc_list.length; i++) {
		if (punc_list[i] == char) {
			return true
		}
	}

	return false
}

function atom(char) {
	if (!isNaN(parseFloat(char))) {
		let parsed = parseFloat(char)
		return {type: "number", value: parsed}
	}
	// else if (char[0] == "'" && char[char.length - 1] == "'") {
	// 	return {type : "string", value : char}
	// }
	else if (char == "TRUE") {
		return {type: "bool", value: char}
	}
	else if (char == "FALSE") {
		return {type: "bool", value: char}
	}
	else {
		return {type : "symbol", value : char}
	}
	console.alert("Something passed through the atom")
}

function ignore_string(array, string) {
	if (string != "") {
		string = string.toUpperCase()
		string = atom(string)
		array.push(string)
	}
}

function tokenize(string) {
	let token_array = new Array()
	let current_word = ""
	let isString = false
	for (let i = 0; i < string.length; i++) {

		if (!isString) {
			if (string[i] == " ") {
				ignore_string(token_array, current_word)
				current_word = ""
			}
			else if (string[i] == "(") {
				ignore_string(token_array, current_word)
				current_word = ""
				token_array.push({type : "punc", value : "("})
			}
			else if (string[i] == ")") {
				ignore_string(token_array, current_word)
				current_word = ""
				token_array.push({type : "punc", value : ")"})
			}
			else if(string[i] == "\t") {
				// do nothing
			}
			else if (string[i] == "\n") {
				// do nothing
			}
			else if ((i + 1) == string.length && string[i] != ")") {
				current_word += string[i]
				ignore_string(token_array, current_word)
			}
			else {
				current_word += string[i]
			}
		}
		else {
			current_word += string[i]
		}

		if (string[i] == '"') {
			if (isString) {
				isString = false
				token_array.push({type : "string", value: current_word.slice(0, current_word.length - 1)})
				current_word = ""
			}
			else {
				isString = true
				current_word = "" 
			}
		}
	}
	return token_array
}

function make_array(array) {
	let string = ""
	for (let i = 0; i < array.length; i++) {
		let current = array[i]
		let next = array[i + 1]
		if (array[i] == undefined) {
			return string
		}
		if (array.length == 1) {
			return array[0]
		}
		else if (current.type != "punc" && next != undefined && next.type != "punc") {
			string += JSON.stringify(current) + ","
		}
		else if (current.type != "punc" && next != undefined && next.value == "(") {
			string += JSON.stringify(current) + ","
		}
		else if (current.value == ")" && next != undefined && next.type != "punc") {
			string += "],"
		}
		else if (current.value == ")" && next != undefined && next.value == "(") {
			string += "],"
		}
		else if (current.value == "(") {
			string += "["
		}
		else if (current.value == ")") {
			string += "]"
		}
		else {
			string += JSON.stringify(current)
		}
	}
	try {
		return eval(string)
	}
	catch(error) {
		console.error(`Error Parenthese Not Matching: ${error}`)
		return [null]
	}
}

let lisp_to_js = {
	'+' : function(array) {
		if (array.length >= 2) {
			let js_string = "("
			for (let i = 0; i < array.length; i++) {
				if ((i + 1) == array.length) {
					js_string += array[i].value + ')'
					return {type: 'expression', value: js_string}
				}
				js_string += array[i].value + ' + '
			}
			return {type: 'expression', value: js_string}
		}
		else {
			console.error("Error: Must have at least two arguments for the function +")
			return null
		}
	}
}


function lisp_eval(expression) {
	if (!(expression instanceof Array)) {
		return expression
	}
	else if (expression[0].type == "symbol" && expression[0].value == "DEFPARAMETER") {
		if (expression[1].type == "symbol") {
			console.log("resultDDD:")
			console.log(expression[2])
			let result = {type: 'expression', value: {lisp: expression[2], js: lisp_eval(expression[2]).value}}
			let js_string = `${expression[1].value} = ${JSON.stringify(result.value)};`
			console.log(js_string)
			return {type: 'expression', value: js_string}
		}
		else {
			console.error("Error: In ${expression[0].value} ${expression[1].value} must be a symbol")
		}
	}
	else if (expression[0].type == "symbol" && expression[0].value.lisp == "LIST") {
		let list = new Array()
		for (let i = 1; i < expression.length; i++) {
			if (expression[i].type == "symbol") {
				let result = eval(expression[i].value)

			}
		}
		console.log(list)
	}
	else if (expression[0]) {
		let args = new Array()
		for (let i = 1; i < expression.length; i++) {
			args.push(expression[i])
		}
		console.log("Args:")
		console.log(args)

		let result = lisp_env[expression[0].value](args)

		console.log("Result:")
		console.log(result)

		return result
	}
}


// run(`(+ 1 2 3 4 5 6)`)
eval(run(`(defparameter hello (+ 3 3))`).value)
console.log("EVALED")
console.log(eval("HELLO"))





















/*---[ PARSER ]---*/
var categorize = function(input) {
  if (!isNaN(parseFloat(input))) {
    return { type:'number', value: parseFloat(input) };
  } else if (input[0] === "^") {
    return {type: 'symbol', value: input.slice(1)}
  } else if (input[0] === '"' && input.slice(-1) === '"') {
    return { type:'string', value: input.slice(1, -1) };
  } else {
    return { type:'identifier', value: input };
  }
};

var parenthesize = function(input, list) {
  if (list === undefined) {
    return parenthesize(input, []);
  } else {
    var token = input.shift();
    if (token === undefined) {
      return list.pop();
    } else if (token === "(") {
      list.push(parenthesize(input, []));
      return parenthesize(input, list);
    } else if (token === ")") {
      return list;
    } else {
      return parenthesize(input, list.concat(categorize(token)));
    }
  }
};

var tokenize = function(input) {
  return input.split('"')
              .map(function(x, i) {
                 if (i % 2 === 0) { // not in string
                   return x.replace(/\(/g, ' ( ')
                           .replace(/\)/g, ' ) ');
                 } else { // in string
                   return x.replace(/ /g, "!whitespace!");
                 }
               })
              .join('"')
              .trim()
              .split(/\s+/)
              .map(function(x) {
                return x.replace(/!whitespace!/g, " ");
              });
};

var parse = function(input) {
  return parenthesize(tokenize(input));
};

/*---[ ENV ]---*/
var Context = function(scope, parent) {
  this.scope = scope;
  this.parent = parent;

  this.get = function(identifier) {
    if (identifier in this.scope) {
      return this.scope[identifier];
    } else if (this.parent !== undefined) {
      return this.parent.get(identifier);
    }
  };
};

var interpret = function(input, context) {
  // if (context === undefined) {
  //   return interpret(input, new Context(library));
  // } else 
  if (input instanceof Array) {
    return lisp_env(input, context);
  } else if (input.type === "identifier") {
    let data = context.get(input.value)
    if (data instanceof Function) {
      return `Error: ${input.value} is not a variable but a function`
    }
    else {
      // console.log("GET:")
      // console.log(input)
      return context.get(input.value);
    }
  } else if (input.type === "number" || input.type === "string" || input.type === "syntax" || input.type === "symbol") { // might have to remove syntax
    return input;
  }
};

var special = {
  'defparameter' : function(input, context) {
    library[input[1].value] = interpret(input[2], context)
  },
  'let' : function(input, context) {
    // console.log("LET:")
    let vars = input[1]
    let new_context = {}
    let body = input[2]
    for (let i = 0; i < vars.length; i++) {
      // console.log(vars[i])
      new_context[vars[i][0].value] =  interpret(vars[i][1], context)
    }
    // console.log(new_context)
    // console.log(body)
    return lisp_env(body, new Context(new_context, context))
  },
  'list' : function(input, context) {
    let new_args = new Array()
    for (let i = 1; i < input.length; i++) {
      new_args.push(interpret(input[i], context))
    }
    return new_args
  },
  'eval' : function(input, context) {
    // console.log("EVAL:")
    // console.log(input)
    let evaled = interpret(input[1], context)
    // console.log("EVALED:")
    // console.log(evaled)
    // console.log("EVALED1:")
    // console.log(interpret(evaled, context))
    return interpret(evaled, context)
  }
}

var library = {
  'meow' : {type: "number", value: 7},
  '+' : function(x) {
    // console.log("X:")
    // console.log(x)
    let sum = 0
    for (let i = 0; i < x.length; i++) {
      if (x[i].type != "number") {
        return `Error: ${x[i].value} is not a number`
      }
      else {
        sum += x[i].value
      }
    }
    return  {type : "number", value: sum}
  }
}

var lisp_env = function(input, context) {
  // console.log("Input:")
  // console.log(input)
  if (context === undefined) {
    return lisp_env(input, new Context(library))
  }
  else if (input[0].value in special) {
    return special[input[0].value](input, context)
  }
  else {
    let args = new Array()
    for (let i = 1; i < input.length; i++) {
      args.push(interpret(input[i], context))
    }
    // console.log("Args:")
    // console.log(args)
    if (args.length != 0) {
      return context.get([input[0].value])(args)
    }
    else {
      return context.get([input[0].value])
    }
  }
}

// console.log(lisp_env(parse("(+ 1 2)")))
// console.log(lisp_env(parse("(+ 1 2 (+ 3 4))")))
// console.log(lisp_env(parse("(+ 1 2 (+ 3 4 (+ 5 5)))")))
// console.log(lisp_env(parse(`(+ 1 "bla")`))) // error message
// console.log(lisp_env(parse(`(defparameter hello meow)`)))
// console.log(lisp_env(parse(`(defparameter hello (list + 1 2))`))) // to do
// console.log(lisp_env(parse(`(defparameter hello (+ 1 2 (+ 3 4)))`)))
// console.log(lisp_env(parse(`(defparameter test 2)`)))
// console.log(lisp_env(parse("(meow)")))
// console.log(lisp_env(parse("(hello)")))
// console.log(lisp_env(parse("(list 1 2 3)")))
// console.log(lisp_env(parse("(list 1 2 hello ^hello)")))
// console.log(lisp_env(parse(`(eval (+ hello test))`)))
// console.log(lisp_env(parse(`(eval (list ^+ 1 2 hello test))`)))
// console.log(lisp_env(parse(`(defparameter syn (list 1 meow))`)))
console.log(lisp_env(parse(`(defparameter syn (list ^+ 1 2 (list ^+ 3 4) 20 meow))`)))
console.log(lisp_env(parse(`(syn)`)))
console.log(lisp_env(parse(`(eval syn)`)))
console.log(lisp_env(parse(`(+ 1 (eval (syn)))`)))
// console.log(lisp_env(parse(`(eval (list syn))`)))
// console.log(lisp_env(parse("(syn)")))
// console.log(lisp_env(parse(`(let ((a 2)
//                                   (b 3)
//                                   (c test))
//                              (+ a b b c test hello))`)))
// console.log(lisp_env(parse(`(a)`)))
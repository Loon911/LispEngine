var run = function(program) {
	return eval_lisp(parse(program),'global_env')
} 

function parse(program) {
	return read_from_tokens(tokenize(program))
}

function tokenize(chars) {
	return chars.replace(/\(/g, ' ( ').replace(/\)/g, ' ) ').trim().match(/(?:[^\s"']+|"[^"]*"|'[^']*')+/g)
	//return chars.replace(/\(/g, ' ( ').replace(/\)/g, ' ) ').trim().split(/\s+/)
}

function read_from_tokens(tokens) {
	if (tokens.length == 0) {
		return "Unexpected EOF while reading"
	}
	var token = tokens.shift()
	if ('(' == token) {
		var L = []
		while (tokens[0] != ")") {
			L.push(read_from_tokens(tokens))
		}
		tokens.shift()
		return L
	}
	else if (')' == token) {
		return "Unexpected"
	}
	else {
		return atom(token)
	}
}

function atom(token) {
	if (!isNaN(parseFloat(token))) {
		return {value: parseFloat(token), type: "number"}
	}
	else if(token[0] == '"' || token[0] == "'") {
		return {value: token, type: "string"}
	}
	else {
		return {value: token, type: "symbol"}	
	}
}

// Make ID:
function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

var env = {
	"ids" : {
		'id' : "ids",
		'+' : {'id' : "e334fa27-a474-4c99-9153-53da88bfde90", 'stored' : "global_env"},
		'-' : {'id' : "0c45a697-3f3b-4718-83a2-844093e59a49", 'stored' : "global_env"},
		'*' : {'id' : "3e8e59cb-a15f-4d07-8de8-b329fdf1f75c", 'stored' : "global_env"},
		'/' : {'id' : "da6c81ae-8220-439e-810b-0801d15339e1", 'stored' : "global_env"},
		'<' : {'id' :"5065e9a1-a79c-470b-8893-1d597d67cb6a", 'stored' : "global_env"},
		'<=' : {'id' : "2de53d28-2f4d-40df-bfe3-2590029ff89f", 'stored' : "global_env"},
		'>' : {'id' : "0c0cbc31-ab9d-4f44-89d8-7923540f4d6c", 'stored' : "global_env"},
		'>=' : {'id' : "7c04a494-955d-4ba2-9816-1594e5c61426", 'stored' : "global_env"},
		'print' : {'id' : "8f75755b-b562-4785-a91b-75b0aaed465a", 'stored' : "global_env"},
		'pi' : {'id' : "aa101bfe-01e0-402b-aa0e-ba4fac303b35", 'stored' : "global_env"},
		'first' : {'id' : "53a3e910-a6bb-4009-b575-7575610fee0e", 'stored' : "global_env"},
		'numberp' : {'id' : "d7af3900-3a94-456b-9045-25258f46b356", 'stored' : "global_env"},
		'symbolp' : {'id' : "70fd965c-078b-4c6b-9c22-5d957513e98b", 'stored' : "global_env"}
	},
	'global_env' : {
		'id' : "global_env",
		'e334fa27-a474-4c99-9153-53da88bfde90' : function(x) { // +
			var sum = x[0]
			for (var i = 1; i < x.length; i++) {
				sum += x[i]
			}
			return sum
		},
		'0c45a697-3f3b-4718-83a2-844093e59a49' : function(x) { // -
			var sum = x[0]
			for (var i = 1; i < x.length; i++) {
				sum = sum - x[i]
			}
			return sum
		},
		'3e8e59cb-a15f-4d07-8de8-b329fdf1f75c' : function(x) { // *
			var sum = x[0]
			for (var i = 1; i < x.length; i++) {
				sum = sum * x[i]
			}
			return sum
		},
		'da6c81ae-8220-439e-810b-0801d15339e1' : function(x) { // /
			var sum = x[0]
			for (var i = 1; i < x.length; i++) {
				sum = sum / x[i]
			}
			return sum
		},
		'5065e9a1-a79c-470b-8893-1d597d67cb6a' : function(x) { // <
			return x[0] < x[1]
		},
		'2de53d28-2f4d-40df-bfe3-2590029ff89f' : function(x) { // <=
			return x[0] <= x[1]
		},
		'0c0cbc31-ab9d-4f44-89d8-7923540f4d6c' : function(x) { // >
			return x[0] > x[1]
		},
		'7c04a494-955d-4ba2-9816-1594e5c61426' : function(x) { // >=
			return x[0] >= x[1]
		},
		'8f75755b-b562-4785-a91b-75b0aaed465a' : function(x) { // print
			return x
		},
		'aa101bfe-01e0-402b-aa0e-ba4fac303b35' : Math.PI, // pi
		'53a3e910-a6bb-4009-b575-7575610fee0e' : function(x) { // first
			return x[0]
		},
		'd7af3900-3a94-456b-9045-25258f46b356' : function(x) { // numberp
			if (!isNaN(x[0]))
				return true
			else
				return false
		},
		'70fd965c-078b-4c6b-9c22-5d957513e98b' : function(x) { // symbolp
			if (typeof x[0] === "string")
				return true
			else
				return false
		}
	},
	'function_env' : {

	},
	'lambda_env' : {

	}
}


function eval_lisp(x,env_id) {
	console.log(x)
	if (!(x instanceof Array)) {
		// do something with the value
		if (x.type == "symbol") {
			var test_query = env.ids[x.value]
			if (test_query != undefined) { // andthing stored in ids will be found
				return env[test_query.stored][test_query.id]
			}
			else {
				return x.value
			}
		}
		else if (x.type == "lambda") {
			console.log(x)
			console.log(env_id)

			

			return "meow"
		}
		else if (x.type == "number") {
			return x.value
		}
		else if (x.type == "string") {
			return x.value
		}
		else { // could be garbage returns whole {}
			return x
		}
	}
	else if (x[0].value == "if") {
		if (eval_lisp(x[1]), env_id) {
			return eval_lisp(x[2], env_id)
		}
		else {
			return eval_lisp(x[3], env_id)
		}
	}
	else if (x[0].value == "defparameter") {
		var new_var = {}
		new_var.id = uuidv4()
		new_var.stored = "global_env"
		env.ids[x[1].value] = new_var
		env.global_env[new_var.id] = eval_lisp(x[2], env_id)
	}
	else if (x[0].value == "setf") {
		var look_up = env.ids[x[1].value]
		env[look_up.stored][look_up.id] = eval_lisp(x[2],env_id)
	}
	else if (x[0].value == "defun") {
		var new_var = {}
		new_var.id = uuidv4()
		new_var.stored = "function_env"
		env.ids[x[1].value] = new_var
		env.function_env[new_var.id] = eval_lisp(x[2], env_id)
	}
	else if (x[0].value == "lambda") {
		var new_lambda_env = {}
		new_lambda_env.type = "lambda"
		new_lambda_env.id = uuidv4()
		new_lambda_env.parent = env_id
		var parameter_list = []
		for (var i = 0; i < x[1].length; i++) {
			var value_eval = x[1][i].value
			parameter_list.push(value_eval)
			new_lambda_env[value_eval] = null
		}
		new_lambda_env.parameter_list = parameter_list
		new_lambda_env.expression = x[2]

		 env.lambda_env[new_lambda_env.id] = new_lambda_env
		 return new_lambda_env
	}
	else {

		////-----//////------- !IMPORTANT MAKE + work first ///////////--///
		var proc = eval_lisp(x[0], env_id)
		// console.log(proc) // diffrent
		// console.log(x[0]) // diffrent
		// if (proc.type == "lambda") {
			
		// }
		if (proc.type == "lambda") {
			if (proc.parameter_list.length != x.length - 1) {
				if (proc.parameter_list.length < x.length)
					return "Too many arguments in call to " + proc.id
				else
					return "Too few arguments in call to " + proc.id
			}

			for (var i = 1; i < x.length; i++) { // set parameter don't need to look up global_env
				var current_parameter = eval_lisp(x[i], env_id)
				var look_up = proc.parameter_list[i - 1]
				proc[look_up] = current_parameter
			}

			//!!!!!!!!!!!!!_____________________ IMPOROTNAT !!!!!!!!!!!!!!______________!!!!!!!!!!!!!!!!!!!//
			// Check it's self first then check parent under lambda in env
			return eval_lisp(proc, proc.id)
		}
		else {
			var args = new Array()
			for (var i = 1; i < x.length; i++) {
				args.push(eval_lisp(x[i],env_id))
			}
			var result = null
			try {
				result = args.length != 0 ? proc(args) : proc
			}
			catch(error) {
				result = "EXECUTE ERROR: " + error + "\n" + proc
			}
			return result
		}
	}
}


var readline = require('readline');
var rl = readline.createInterface({

  input: process.stdin,
  output: process.stdout,
  terminal: false,
  prompt: "? "
});

rl.prompt()

rl.on('line', function(line){
    console.log(run(line.toString()));
    rl.prompt()
})











